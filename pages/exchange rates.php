<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/25/2015
 * Time: 9:43 PM
 */
$admin=isset($admin)?$admin:false;
$baseLink=isset($baseLink)?$baseLink:'';
loadModel('ExchangeRate');
$model = new ExchangeRate();
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'index';
if($admin)
    $model = processFormRequest($model, $action, $baseLink);
$labels = $model->getAttributes();
$data = $model->getAll();
$errors = $model->getErrors();?>

<div class="errors text-center">
    <?php if(!empty($errors))
        foreach($errors as $error)
            echo "<p>$error</p>"?>
</div>

<?php if($admin) echo "<form method='post' action='$baseLink&action=update'>"?>
<table class="table table-responsive table-bordered">
    <thead>
    <tr>
        <th rowspan="2" valign="middle">
            <?php if($admin){?>
                <button class="btn btn-success" type="submit">Update</button>
            <?php }else{?>
                Forex bureaus
            <?php }?>
        </th>
        <?php foreach($data['currencies'] as $currency)
            echo "<th colspan='2' class='text-center'>{$currency[$admin?'code':'name']}</th>"?>
    </tr>
    <tr>
        <?php foreach($data['currencies'] as $currency):?>
            <th class='text-center'><?php echo $labels['rate_buying']?></th>
            <th class='text-center'><?php echo $labels['rate_selling']?></th>
        <?php endforeach?>
    </tr>
    </thead>
    <tbody>
        <?php foreach($data['forexBureaus'] as $forexBureau):?>
            <tr>
                <th><?php echo $forexBureau['name']?></th>
                <?php foreach($forexBureau['currencies'] as $forexCurrency):?>
                    <?php $bureau_id = $forexBureau['id']?>
                    <?php $currency_id = $forexCurrency['id']?>
                    <?php $rate = array(
                        'buy'=>$forexCurrency['rate']['rate_buying'],
                        'sell'=>$forexCurrency['rate']['rate_selling'],
                        'id'=>$forexCurrency['rate']['id']
                    )?>
                    <td class='text-center'>
                        <?php if($admin){?>
                            <input type="hidden"
                                   name="<?php echo "ExchangeRate[$bureau_id][$currency_id][id]" ?>"
                                   value="<?php echo $rate['id']?>"
                                >
                            <input type="text" class="form-control"
                                   value="<?php echo $rate['buy']?>"
                                   name="<?php echo "ExchangeRate[$bureau_id][$currency_id][rate_buying]" ?>" required>
                        <?php }else echo $rate['buy']?>
                    </td>
                    <td class='text-center'>
                        <?php if($admin){?>
                            <input type="text" class="form-control"
                                   value="<?php echo $rate['sell']?>"
                                   name="<?php echo "ExchangeRate[$bureau_id][$currency_id][rate_selling]" ?>" required>
                        <?php }else echo $rate['sell']?>
                    </td>
                <?php endforeach?>
            </tr>
        <?php endforeach?>
    </tbody>
</table>
<?php if($admin) echo '</form>'?>
