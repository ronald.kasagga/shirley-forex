    <?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/24/2015
 * Time: 5:42 PM
 */
    $errors = array();
    $admin = './?p=admin';

    if(Session::isSignedIn())
        header("Location: $admin");
    if(isset($_POST['Login'])){
        $username = $_POST['Login']['username'];
        $password = $_POST['Login']['password'];
        require_once __DIR__ . '/../models/User.php';
        $user = new User($username, $password);
        if($user->login())
            header("Location: $admin");
        else
            $errors = $user->getErrors();
    }?>
    <div class="container col-md-offset-4 col-md-4">
        <form class="form-signin" method="post">
            <div class="center-block">
                <img style="width: 150px" src="<?php echo $config['app-icon']?>" class="center-block">
            </div>

            <h3 class="form-signin-heading text-center">Sign In</h3>

            <div class="login-errors text-center">
                <?php if(!empty($errors))
                    foreach($errors as $error)
                        echo "<p>$error</p>"?>
            </div>

            <div class="form-group">
                <label for="inputUsername" class="sr-only">Username</label>
                <input type="text" id="inputUsername" class="form-control" name="Login[username]" placeholder="Username" required autofocus>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" class="form-control" name="Login[password]" placeholder="Password" required>
            </div><!--
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>-->
            <div class="form-group">
                <button class="btn btn-success" type="submit"><i class="glyphicon glyphicon-lock"></i> Sign In</button>
                <a href="<?php echo $navigation['pages'][0]['url']?>"
                   class="btn btn-default"><i class="glyphicon glyphicon-home"></i> Home</a>
            </div>
        </form>
    </div>
