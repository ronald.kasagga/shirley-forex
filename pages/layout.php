<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/23/2015
 * Time: 6:35 PM
 */?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Forex Bureau and Rates information">
        <meta name="author" content="Nakalema Shirley">
        <link rel="icon" href="favicon.ico">

        <title><?php echo ucwords($navigation['page']) .' - '. $config['app-name']?></title>

        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/main.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <?php include_once __DIR__ .'/navigation.php'?>

        <div class="container">
            <?php if($navigation['page'] != 'admin'):?>
                <h3 class="text-center"><?php echo ucwords($navigation['page'])?></h3>
            <?php endif?>
            <?php loadPage($navigation['page'])?>
        </div> <!-- /container -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>

