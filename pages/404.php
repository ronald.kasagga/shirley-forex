<!-- Main component for a primary marketing message or call to action -->
<div class="jumbotron center-block">
    <div class="text-center">
        <h1>404</h1>
        <h3>Page Not found</h3>
        <p>
            <a class="btn btn-lg btn-primary" href="<?php echo $navigation['pages'][0]['url']?>" role="button">
                <i class="glyphicon glyphicon-home"></i> Back to home </a>
        </p>
    </div>
</div>