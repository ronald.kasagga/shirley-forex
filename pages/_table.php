<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/25/2015
 * Time: 6:07 PM
 * @var Model $model
 * @var array $data
 */
$admin=isset($admin)?$admin:false;
$attributes = $model->getAttributes();
if($admin)
    $attributes['actions'] = 'Actions';
$index = array_keys($attributes);
$data = isset($data) ? $data : array()?>

<table class="table table-responsive">
    <thead>
        <tr><?php foreach($attributes as $attribute)
            echo "<th>$attribute</th>"?>
        </tr>
    </thead>
    <tbody>
        <?php if(!empty($data)):?>
            <?php foreach($data as $row):?>
            <tr>
                <?php foreach($index as $key) echo "<td>{$row[$key]}</td>"?>
            </tr>
            <?php endforeach?>
        <?php endif?>
    </tbody>
</table>

