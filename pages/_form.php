<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/25/2015
 * Time: 6:40 PM
 * @var Model $model
 * @var string $action
 */
$class = get_class($model);
$errors = $model->getErrors();
$data = isset($data) ? $data : array();
$isNewRecord = empty($data);
$buttonLabel = $isNewRecord ? 'Create' : 'Update';
$title = ($isNewRecord ? 'Add' : 'Edit') . ' '. beautifyCodedPhrase($class)?>

<form class="form-signin" method="post" action="<?php echo $action?>">
    <h3 class="form-signin-heading text-center"><?php echo ucwords($title)?></h3>

    <div class="errors text-center">
        <?php if(!empty($errors))
            foreach($errors as $error)
                echo "<p>$error</p>"?>
    </div>

    <?php foreach($model->getAttributes() as $attribute => $attributeLabel):?>
        <?php
            $id = "input$attribute";
            $name = ucfirst($class)."[$attribute]";
            $value = isset($data[$attribute]) ? $data[$attribute] : null;
        ?>
        <div class="form-group">
            <label for="<?php echo $id?>" class="sr-only"><?php echo $attributeLabel?></label>
            <input type="text"
                   id="<?php echo $id?>"
                   class="form-control" value="<?php echo $value?>"
                   name="<?php echo $name?>" placeholder="<?php echo $attributeLabel?>" required>
        </div>
    <?php endforeach?>
    <div class="form-group">
        <button class="btn btn-success" type="submit"><?php echo $buttonLabel?></button>
    </div>
</form>
