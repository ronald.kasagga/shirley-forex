<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/25/2015
 * Time: 9:12 PM
 */
$admin=isset($admin)?$admin:false;
$baseLink=isset($baseLink)?$baseLink:'';
loadModel('ForexBureau');
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'create';
$model = new ForexBureau();
$model = processFormRequest($model, $action, $baseLink)?>

<div class="row">
    <div class="col-md-<?php echo $admin ? 8 : 12?>">
        <?php loadPage('_table', array(
            'model'=>$model,
            'admin'=>$admin,
            'data'=>$model->getAll(array(),'AND',array('isAdmin'=>$admin, 'baseLink'=>$baseLink))))?>
    </div>

    <?php if($admin):?>
        <div class="col-md-4">
            <?php
            $formAction = "$baseLink&action=$action".($action=='edit'?'&id='.$_REQUEST['id']:'');
            $data = ($action=='edit') ? $model->getOne(['id'=>$_REQUEST['id']]):array();

            loadPage('_form', array(
                'model'=>$model,
                'action' => $formAction,
                'data'=>$data
            ));?>
        </div>
    <?php endif?>
</div>