<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/24/2015
 * Time: 8:29 PM
 */
if(!Session::isSignedIn())
    header('Location: ./?p=login');
$sections = array('currencies', 'forex bureaus', 'exchange rates',);
$baseLink = './?p=admin';
$section = isset($_REQUEST['section']) ? $_REQUEST['section'] : $sections[0]?>

<div class="">
    <ul class="nav nav-pills nav-stacked col-md-2">
        <?php foreach($sections as $sec):?>
            <?php $active = ($section == $sec) ? 'class="active"' : ''?>
            <li <?php echo $active?>>
                <a href="<?php echo "$baseLink&section=$sec"?>" ><?php echo ucwords($sec)?></a>
            </li>
        <?php endforeach?>
    </ul>
    <div class="tab-content col-md-10">
        <h3 class="text-center"><?php echo ucwords('admin - '.$section)?></h3>
        <hr>
        <div class="tab-pane active" id="tab-admin">
            <?php loadPage($section, array('baseLink'=>$baseLink."&section=$section", 'admin'=>true))?>
        </div>
    </div>
</div>
