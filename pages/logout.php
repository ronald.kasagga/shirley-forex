<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/24/2015
 * Time: 8:23 PM
 */
echo 'Logging out';
if(Session::isSignedIn())
    Session::destroy();
header('Location: ./?p='.$navigation['pages'][0]['url']);