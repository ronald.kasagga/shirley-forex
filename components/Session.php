<?php

/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/23/2015
 * Time: 7:15 PM
 */
class Session
{
    public static function start() {
        if (!session_id())
            session_start();
    }

    public static function set($index, $value) {
        self::start();
        $_SESSION[$index] = $value;
    }

    public static function un_set($index) {
        self::start();
        unset($_SESSION[$index]);
    }

    public static function destroy() {
        if (session_id() != "" || isset($_COOKIE[session_name()])) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        }
        session_unset();
        $_SESSION = array();
        session_destroy();
    }

    public static function get($index) {
        self::start();
        if (isset($_SESSION[$index]))
            return $_SESSION[$index];

        return FALSE;
    }

    public static function isSignedIn()
    {
        return self::get('loggedIn');
    }

    public static function startUserSession($user)
    {
        if(empty($user))
            return false;

        $data = array(
            'name'=>$user['name'],
            'username'=>$user['username'],
            'user_id'=>$user['id'],
            'login_time'=>date('Y-m-d H:i:s'),
            'loggedIn'=>true
        );
        foreach($data as $key => $value)
            self::set($key, $value);

        return true;
    }
}