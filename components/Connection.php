<?php

/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/24/2015
 * Time: 6:27 PM
 *
 * @property mysqli $dbConnection
 */
class Connection {

    private static $instance;
    private static $configuration;
    private $dbConnection;
    private $_errors;

    private function __construct() {
        $this->_errors = array();
    }

    private static function getInstance(){
        if (self::$instance == null)
            self::$instance = new self();
        
        return self::$instance;
    }

    /**
     * @return self
     */
    private static function initConnection(){
        $db = self::getInstance();
        $config = self::getConfiguration();
        @$db->dbConnection = new mysqli($config['host'], $config['username'], $config['password'],$config['database']);
        if($db->dbConnection->connect_error){
            $db->_errors[] = $db->dbConnection->connect_error;
            $db->_errors[] =
                '<script>setTimeout(function(){
                    alert('. '"Please check the configuration at ' .str_replace('\\', '\\\\', realpath(__DIR__.'/../config/db.php')).' and try again")
                }, 1);</script>';
        }
        else
            $db->dbConnection->set_charset($config['charset']);

        return $db;
    }

    /**
     * @return mysqli|null
     */
    public static function getConnection() {
        try {
            $db = self::initConnection();
            return $db->dbConnection;
        } catch (Exception $ex) {
            self::getInstance()->_errors[] = "Unable to open a connection to the database. " . $ex->getMessage();
            return null;
        }
    }

    private static function getConfiguration()
    {
        if(!isset(self::$configuration))
            self::$configuration = include __DIR__ . '/../config/db.php';

        return self::$configuration;
    }

    public static function getErrors()
    {
        return self::getInstance()->_errors;
    }
}