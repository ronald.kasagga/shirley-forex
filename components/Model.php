<?php

/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/24/2015
 * Time: 6:37 PM
 */
include_once __DIR__ .'/Connection.php';

abstract class Model
{
    protected $_errors;

    protected abstract function getTableName();

    public abstract function getAttributes();

    public function create($data)
    {
        $fields = array_keys($data);
        $values = [];
        foreach($data as $value)
            $values[] = "'$value'";
        $query = 'INSERT INTO ' . $this->getTableName()
            .'(' . implode(',', $fields) . ') VALUES (' . implode(',', $values).');';

        return $this->query($query);
    }

    /**
     * @param array $data
     * @param array $condition
     * @param string $separator
     * @return array
     */
    public function update($data, $condition = array(), $separator = 'AND')
    {
        $set = array();
        foreach($data as $column => $value)
            $set[] = "$column='$value'";

        $query = 'UPDATE ' . $this->getTableName() . ' SET '. implode(', ',$set);
        if(!empty($condition)){
            $where=array();
            foreach($condition as $column => $value)
                $where[] = "$column='$value'";
            $query.=" WHERE ".implode(" $separator ", $where);
        }

        return $this->query($query);
    }

    public function insert($data)
    {
        if(empty($data))
            return false;

        $values = array();

        $query = 'INSERT INTO ' . $this->getTableName() . '(' . implode(', ', array_keys($data)) . ') VALUES ';
        foreach($data as $column => $value)
            $values[] = "'$value'";

        $query .= '(' . implode(',', $values) . ')';

        return $this->query($query);
    }

    public function delete($condition, $separator='AND')
    {
        $query = 'DELETE FROM ' . $this->getTableName();
        $where = array();
        foreach($condition as $column => $value)
            $where[] = "$column='$value'";

        $query .= ' WHERE ' . implode(" $separator ", $where);

        return $this->query($query);
    }

    protected function addError($error){
        if($this->_errors == null)
            $this->_errors = array();

        $this->_errors[] = $error;
    }

    public function getErrors(){
        return $this->_errors;
    }

    protected function hasErrors(){
        return !empty($this->getErrors());
    }

    public function getOne($condition = array(), $separator = 'AND'){
        $query = $this->buildQueryForCondition($condition, $separator) . ' LIMIT 1';
        return $this->query($query);
    }

    public function getAll($condition = array(), $separator = 'AND', $options = array()){
        $query = $this->buildQueryForCondition($condition, $separator);
        $records = $this->query($query, true);
        if(!empty($records) && !empty($options))
            $records = $this->processOptions($records, $options);
        return $records;
    }

    /**
     * @param $query
     * @param bool $asList
     * @return array
     */
    public function query($query, $asList = false)
    {
        @$connection = Connection::getConnection();
        $this->_errors = Connection::getErrors();

        if(empty($this->_errors)){
            $isDDl = (bool) preg_match("/^(insert|update|delete)/", strtolower($query));
            $result = $connection->query($query);

            if($connection->errno != 0)
                $this->_errors[] = $connection->error;
            if($isDDl)
                return $result;
            else if($result->num_rows == 1)
                return $asList ? array($result->fetch_assoc()) : $result->fetch_assoc();
            else if($result->num_rows > 1)
                return $result->fetch_all(MYSQLI_ASSOC);
        }

        return array();
    }

    private function buildQueryForCondition($condition = array(), $separator = 'AND')
    {
        $query = "SELECT * FROM " . $this->getTableName();
        if(!empty($condition)){
            $query .= ' WHERE ';
            $conditions = array();
            foreach($condition as $property => $value)
                $conditions[] = " $property='$value' ";
            $query .= implode($separator, $conditions);
        }

        return $query;
    }

    private function processOptions($records, $options)
    {
        if(isset($options['isAdmin']) && $options['isAdmin']){
            foreach($records as $index => $record)
                $records[$index]['actions'] = $this->getModelActions($record['id'], $options['baseLink']);
        }
        return $records;
    }

    private function getModelActions($model_id, $baseUrl)
    {
        $actions = array(
            'edit'=>'<i class="glyphicon glyphicon-pencil"></i>',
            'delete'=>'<i class="glyphicon glyphicon-trash"></i>',
            );
        $html = '';
        foreach($actions as $name => $icon)
            $html .= "<a href='$baseUrl&action=$name&id=$model_id' title='$name'>$icon</a> ";

        return $html;
    }
}