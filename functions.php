<?php

function loadPage($page, $options = array()){
    $page =  __DIR__ . '/pages/'.$page .'.php';
    if(file_exists($page)){
        if(!empty($options))
            foreach ($options as $property => $value)
                ${$property} = $value;
        include_once $page;
    }
    else
        include_once __DIR__ . '/pages/404.php';
}

function beautifyCodedPhrase($phrase){
    $words = preg_split("/[\s,-_.,]+/", strtolower($phrase));
    return ucwords(implode(' ', $words));
}

function loadModel($model){
    include_once __DIR__ . '/models/'.ucfirst($model).'.php';
}

function processFormRequest(Model $model, $action, $returnUrl){
    $class = get_class($model);
    switch($action){
        case 'create':
            if(isset($_POST[$class])){
                $data = $_POST[$class];
                if($model->create($data))
                    header("Location: $returnUrl");
            }
            break;
        case 'update':
            if(isset($_POST[$class])){
                $data = $_POST[$class];
                $updated = $model->update($data);
                if($updated)
                    header("Location: $returnUrl");
            }
            break;
        case 'edit':
            if(isset($_POST[$class])){
                $data = $_POST[$class];
                $edited = $model->update($data, array('id'=>$_REQUEST['id']));
                if($edited)
                    header("Location: $returnUrl");
            }
            break;
        case 'delete':
            $edited = $model->delete(array('id'=>$_REQUEST['id']));
            if($edited)
                header("Location: $returnUrl");
            break;
    }
    return $model;
}