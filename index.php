<?php
/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/23/2015
 * Time: 6:10 PM
 */

/**
 * @param string $page
 * @param array $options
 */
include_once __DIR__ .'/functions.php';

$navigation['pages'] = array(
    //array('name'=>'home', 'url'=>'./?p=home'),
    array('name'=>'exchange rates', 'url'=>'./?p=exchange rates'),
    array('name'=>'forex bureaus', 'url'=>'./?p=forex bureaus'),
    array('name'=>'admin', 'url'=>'./?p=admin'),
);

$navigation['page'] = isset($_REQUEST['p']) ? $_REQUEST['p'] : 'home';

if($navigation['page'] =='login')
    $navigation['pages'][] = array('name'=>'login', 'url'=>'./?p=login');

include_once __DIR__ .'/config/main.php';
include_once __DIR__ .'/components/Session.php';
include_once __DIR__ .'/pages/layout.php';



