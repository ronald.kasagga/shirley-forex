<?php

/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/25/2015
 * Time: 9:14 PM
 */
include_once __DIR__ . '/../components/Model.php';
class ForexBureau extends Model
{

    protected function getTableName()
    {
        return 'forex_bureau';
    }

    public function getAttributes()
    {
        return array(
            'name'=>'Name',
            'location'=>'Location',
        );
    }

    public function delete($condition){
        include_once __DIR__ .'/ExchangeRate.php';
        $exchangeModel = new ExchangeRate();
        $exchangeModel->delete(array('bureau_id'=>$condition['id']));
        return parent::delete($condition);
    }
}