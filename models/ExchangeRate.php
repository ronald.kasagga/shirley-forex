<?php

/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/25/2015
 * Time: 9:49 PM
 */
include_once __DIR__ . '/../components/Model.php';
class ExchangeRate extends Model
{

    private $_rates;

    protected function getTableName()
    {
        return 'exchange_rate';
    }

    public function getAttributes()
    {
        return array(
            'bureau_id'=>'Forex Bureau',
            'currency_id'=>'Currency',
            'rate_buying'=>'Buy',
            'rate_selling'=>'Sell',
        );
    }

    public function getAll($condition = array(), $separator = 'AND'){
        include_once __DIR__ . '/ForexBureau.php';
        include_once __DIR__ . '/Currency.php';

        $forexModel = new ForexBureau();
        $currencyModel = new Currency();

        $forexBureaus = $forexModel->getAll();
        $currencies = $currencyModel->getAll();

        $data = array(
            'currencies'=>$currencies,
            'labels'=>array(
                'forex'=>$forexModel->getAttributes(),
                'currency'=>$currencyModel->getAttributes(),
            ),
            'forexBureaus'=>array()
        );

        foreach($forexBureaus as $index => $bureau){
            $bureau['currencies'] = array();
            foreach($currencies as $currency){
                $currency['rate'] = $this->getRate(array(
                    'bureau_id'=>$bureau['id'], 'currency_id'=>$currency['id'])
                );
                $bureau['currencies'][] = $currency;
            }
            $data['forexBureaus'][] = $bureau;
        }

        return $data;
    }

    private function getRate($condition)
    {
        $rates = $this->getAllRates();
        if(!empty($rates)){
            if(isset($rates[$condition['bureau_id']][$condition['currency_id']]))
                return $rates[$condition['bureau_id']][$condition['currency_id']];
        }
        return array('rate_buying'=>0,'rate_selling'=>0,'id'=>'-1');
    }

    public function update($data, $condition = array())
    {
        foreach($data as $bureau_id => $currency){
            foreach($currency as $currency_id => $rates){
                $data = array('rate_buying'=>$rates['rate_buying'],'rate_selling'=>$rates['rate_selling'],);
                $condition = array('bureau_id'=>$bureau_id, 'currency_id'=>$currency_id);
                if($rates['id'] > 0)
                    parent::update($data, $condition);
                else
                    $this->insert(array_merge($data, $condition));
            }
        }
    }

    private function getAllRates()
    {
        if($this->_rates == null){
            $this->rates = [];
            $rates = parent::getAll();
            if(!empty($rates)){
                foreach ($rates as $rate)
                    $this->_rates[$rate['bureau_id']][$rate['currency_id']] = $rate;
            }
        }

        return $this->_rates;
    }
}