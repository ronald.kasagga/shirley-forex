<?php

/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/24/2015
 * Time: 6:20 PM
 */
require_once __DIR__ . '/../components/Model.php';

class User extends Model
{
    public $username;
    public $password;
    public $name;

    public function __construct($username, $password){
        $this->username = $username;
        $this->password = $password;
        $this->errors = array();
    }

    public function login(){
        $user = $this->getOne(array(
            'username'=>$this->username
        ));

        if($this->hasErrors())
            return false;

        if(empty($user)){
            $this->addError('invalid username');
            return false;
        }

        if($user['password'] != $this->password){
            $this->addError('invalid password');
            return false;
        }

        return Session::startUserSession($user);
    }

    public function getTableName()
    {
        return 'user';
    }

    public function getAttributes()
    {
        return array(
            'name'=>'Name',
            'username'=>'Username',
        );
    }
}