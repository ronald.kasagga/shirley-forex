<?php

/**
 * Created by PhpStorm.
 * User: ronald
 * Date: 11/25/2015
 * Time: 6:17 PM
 */
include_once __DIR__ .'/../components/Model.php';

class Currency extends Model
{

    protected function getTableName()
    {
        return 'currency';
    }

    public function getAttributes()
    {
        return array(
            'name'=>'Name',
            'code'=>'Code',
        );
    }

    public function delete($condition){
        include_once __DIR__ .'/ExchangeRate.php';
        $exchangeModel = new ExchangeRate();
        $exchangeModel->delete(array('currency_id'=>$condition['id']));
        return parent::delete($condition);
    }
}